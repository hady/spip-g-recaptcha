<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'g_recaptcha_description' => 'Ajoute un recaptcha à vos formulaires pour éviter les spams',
	'g_recaptcha_nom' => 'Google Recaptcha',
	'g_recaptcha_slogan' => '',
);
