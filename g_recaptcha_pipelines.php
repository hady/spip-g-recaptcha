<?php
/**
 * Définit les pipelines du plugin Google Recaptcha
 *
 * @plugin     Google Recaptcha
 * @copyright  2017
 * @author     Hadrien
 * @licence    GNU/GPL
 * @package    SPIP\G_recaptcha\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


function g_recaptcha_insert_head($flux) {

	$flux .= '<script src="https://www.google.com/recaptcha/api.js" async defer></script>'. "\n";

	return $flux;
}
