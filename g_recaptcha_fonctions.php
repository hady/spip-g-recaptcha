<?php
/**
 * Fonctions utiles au plugin Google Recaptcha
 *
 * @plugin     Google Recaptcha
 * @copyright  2017
 * @author     Hadrien
 * @licence    GNU/GPL
 * @package    SPIP\G_recaptcha\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function g_recaptcha_verif($code, $ip = null){
	if (empty($code)) {
		return false; // Si aucun code n'est entré, on ne cherche pas plus loin
	}

	include_spip('inc/config');

	$params = [
	'secret'    => lire_config('g_recaptcha/g_recaptcha_key_private'),
	'response'  => $code
	];
	if( $ip ){
		$params['remoteip'] = $ip;
	}
	$url = "https://www.google.com/recaptcha/api/siteverify?" . http_build_query($params);
	if (function_exists('curl_version')) {
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_TIMEOUT, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // Evite les problèmes, si le ser
		$response = curl_exec($curl);
	} else {
		// Si curl n'est pas dispo, un bon vieux file_get_contents
		$response = file_get_contents($url);
	}

	if (empty($response) || is_null($response)) {
		return false;
	}

	$json = json_decode($response);
	return $json->success;
}
