## Utilisation du plugin Google reCAPTCHA

#### Création des clés de l'API reCAPTCHA

 Ca se passe ici avec un compte google [https://www.google.com/recaptcha/admin](https://www.google.com/recaptcha/admin)

 /!\ Renseigner les domaines autorisés pour l'utilisation du reCAPTCHA

#### Configuration du plugin

 Après avoir installé le plugin renseigner la clé publique et la clé privée dans
/ecrire/?exec=configurer_g_recaptcha

#### Ajout dans le formulaire CVT

    #MODELE{g_recaptcha}

 Ainsi que le retour en cas d'erreur

    [<p class='erreur_form'>(#ENV{erreurs/g-recaptcha-response})</p>]

#### Dans la partie vérifier du formulaire CVT ajouter la vérification suivante

    if (!g_recaptcha_verif(_request('g-recaptcha-response')) == 1) {
	    $erreurs['g-recaptcha-response'] = 'Vérification recaptcha invalide';
    }
